module.exports = {
  extends: 'stylelint-config-standard',
  rules: {
    'at-rule-no-unknown': [ true, {
      ignoreAtRules: ['extend', 'at-root', 'debug', 'warn', 'error', 'if', 'else', 'for', 'each', 'while', 'mixin', 'include', 'content', 'return', 'function']
    }],
    'rule-empty-line-before': 'always',
    'selector-pseudo-element-colon-notation': 'single',
    'no-descending-specificity': null,
  }
};
