# CSV Parse Service

My test task for AgileEngine.
Based on my first self-written boilerplate.

### See here: https://csv-parser-c6428.firebaseapp.com

## Start

1) npm i
2) npm start

## Build

1) npm i
2) npm run build


## Lint

1) npm i
2) npm run lint