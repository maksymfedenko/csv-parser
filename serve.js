const server = require('node-static');

const fileServer = new server.Server('./build');
require('http').createServer((request, response) => {
  request.addListener('end', () => {
    fileServer.serve(request, response);
  }).resume();
}).listen(process.env.PORT || 4000);
