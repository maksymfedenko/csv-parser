import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { ErrorsBox, TextArea, Button, Form } from 'components';
import { updateInputtedData, submit } from 'reducers/csv';
import styles from './CsvForm.scss';

export const CsvForm = ({
  isSubmitting,
  isSubmitted,
  inputtedData,
  error,
  updateInputtedData,
  submit,
}) => (
  <section className={styles.CsvForm}>
    <ErrorsBox error={error} />
    <Form>
      <TextArea
        value={inputtedData}
        onChange={updateInputtedData}
        label="CSV Input"
        placeholder="Enter CSV data here..."
        rows={4}
      />
      <Button disabled={isSubmitting || isSubmitted} onClick={submit}>Parse CSV</Button>
    </Form>
  </section>
);

CsvForm.propTypes = {
  isSubmitting: PropTypes.bool.isRequired,
  isSubmitted: PropTypes.bool.isRequired,
  inputtedData: PropTypes.string.isRequired,
  error: PropTypes.string.isRequired,
  updateInputtedData: PropTypes.func.isRequired,
  submit: PropTypes.func.isRequired,
};

const enhance = connect(
  (state) => ({
    isSubmitted: state.csv.isSubmitted,
    isSubmitting: state.csv.isSubmitting,
    inputtedData: state.csv.inputtedData,
    error: state.csv.error,
  }),
  { updateInputtedData, submit },
);

export default enhance(CsvForm);
