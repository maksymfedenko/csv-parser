import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Table } from 'components';
import { changeSort } from 'reducers/csv';
import isEmpty from 'utils/isEmpty';

export const CsvTable = ({
  isSubmitted,
  parsedData,
  currentSort,
  changeSort,
}) => (
  <Table
    isTableShown={isSubmitted && !isEmpty(parsedData)}
    columnNames={parsedData.columns}
    data={parsedData.data}
    sort={currentSort}
    onSort={changeSort}
  />
);

CsvTable.propTypes = {
  isSubmitted: PropTypes.bool.isRequired,
  parsedData: PropTypes.shape({
    columns: PropTypes.array,
    data: PropTypes.array,
  }).isRequired,
  currentSort: PropTypes.shape({
    columnIndex: PropTypes.number,
    order: PropTypes.oneOf(['asc', 'desc']),
  }).isRequired,
  changeSort: PropTypes.func.isRequired,
};

const enhance = connect(
  (state) => ({
    isSubmitted: state.csv.isSubmitted,
    parsedData: state.csv.parsedData,
    currentSort: state.csv.currentSort,
  }),
  { changeSort },
);

export default enhance(CsvTable);
