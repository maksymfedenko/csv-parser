import isEmpty from 'utils/isEmpty';
// needed for commented validation (description below) GOTO: line 9
// import { getRows, getCells } from 'utils/parseCsv';

export default (data) => {
  if (isEmpty(data)) return 'Error: Field is required';
  if ((data.match(/"/g) || []).length % 2 === 1) return 'Error: odd count off "';

  /**
   * Validation for cell's count in row shouldn't be more columnsCount
   * currently handled in Table component (didn't render reduntant elements)
   * That's why turned off
   */

  // const rows = getRows(data);
  // const columnsCount = getCells(rows[0]).length;
  // const rowsWithData = rows.slice(1);

  // const rowHasTooManyCellsIndex = rowsWithData.findIndex(
  //   (row) => getCells(row).length > columnsCount);

  // if (rowHasTooManyCellsIndex !== -1) {
  //   // +2 because columnNames row inputted in textarea too
  // return `${rowHasTooManyCellsIndex + 2}-th row has too many cells (more than ${columnsCount})`;
  // }

  return '';
};
