export default (data, { columnIndex, order }) => {
  const sortableColumn = data.map((row) => row[columnIndex]);
  const sortType = findSortType(sortableColumn);
  // slice needed to avoid mutation of data
  const sortedData = data.slice(0).sort(sortsByType[sortType](columnIndex));
  return order === 'asc' ? sortedData : sortedData.reverse();
};

const isNumber = (value) => !isNaN(parseFloat(value)) && isFinite(value);

const isDate = (value) => !isNaN(new Date(value).valueOf());

const findSortType = (column) => {
  if (column.every(isDate)) return 'date';
  if (column.every(isNumber)) return 'number';
  return 'string';
};

const sortsByType = {
  date: (columnIndex) => (a, b) => {
    const aDateNumber = new Date(a[columnIndex]).getTime();
    const bDateNumber = new Date(b[columnIndex]).getTime();
    if (aDateNumber < bDateNumber) return -1;
    if (aDateNumber > bDateNumber) return 1;
    return 0;
  },
  number: (columnIndex) => (a, b) => {
    const aNumber = parseFloat(a[columnIndex]);
    const bNumber = parseFloat(b[columnIndex]);
    if (aNumber < bNumber) return -1;
    if (aNumber > bNumber) return 1;
    return 0;
  },
  string: (columnIndex) => (a, b) => {
    if (a[columnIndex] < b[columnIndex]) return -1;
    if (a[columnIndex] > b[columnIndex]) return 1;
    return 0;
  },
};
