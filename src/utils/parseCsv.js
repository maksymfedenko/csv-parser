const ROW_REGEX = /[^"\n]*("[^"]*"[^"\n]*)*(\n|$)/g;
const CELL_REGEX = /[^",]*("[^"]*"[^",]*)*(,|$)/g;

export default (data) => {
  const rows = getRows(data);
  const columns = getCells(rows[0]);
  const rowsWithData = rows.slice(1);

  return {
    columns: columns.map(removeDoubleQuotes),
    data: parseRows(rowsWithData, columns.length),
  };
};

export const getRows = (data) => { // eslint-disable-line arrow-body-style
  return (data.match(ROW_REGEX) || [])
    .filter((str) => str)
    .map((str) => removeSymAtEnd(str, '\n'));
};

export const getCells = (row) => { // eslint-disable-line arrow-body-style
  // slice(0, -1) needed to remove last empty element
  return (row.match(CELL_REGEX) || []).slice(0, -1).map((str) => removeSymAtEnd(str, ','));
};

const removeSymAtEnd = (str, sym) => {
  if (str[str.length - 1] === sym) return str.substr(0, str.length - 1);
  return str;
};

const parseRows = (rows) => {
  const parsedRows = rows.map(getCells);
  return parsedRows.map((row) => row.map(removeDoubleQuotes));
};

const removeDoubleQuotes = (cell) => cell.replace(/"([^"]|$)|"{2}$/g, '$1');
