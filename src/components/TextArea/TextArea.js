import PropTypes from 'prop-types';
import cn from 'classnames';
import styles from './TextArea.scss';

class TextArea extends React.PureComponent { // eslint-disable-line no-undef
  onChange = (event) => {
    this.props.onChange(event.target.value);
  };

  render() {
    const { className, value, autofocus, label, placeholder, rows } = this.props;

    return (
      <div className={cn(styles.TextAreaWrapper, className)}>
        {!!label && <label>{label}</label>}
        <textarea
          onChange={this.onChange}
          autoFocus={autofocus}
          placeholder={placeholder}
          defaultValue={value}
          rows={rows}
        />
      </div>
    );
  }
}

TextArea.propTypes = {
  value: PropTypes.string,
  label: PropTypes.string,
  placeholder: PropTypes.string,
  className: PropTypes.string,
  onChange: PropTypes.func,
  autofocus: PropTypes.bool,
  rows: PropTypes.number,
};

TextArea.defaultProps = {
  value: '',
  className: '',
  label: '',
  placeholder: '',
  autofocus: false,
  onChange: () => {},
  rows: 3,
};

export default TextArea;
