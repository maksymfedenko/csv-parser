import PropTypes from 'prop-types';
import cn from 'classnames';
import styles from './Container.scss';

/**
 * Simple container like bootstrap's .container
 */
const Container = ({ children, className, type }) => (
  <section className={cn(styles.Container, styles[type], className)}>
    {children}
  </section>
);

Container.propTypes = {
  type: PropTypes.oneOf(['default', 'full']),
  children: PropTypes.node,
  className: PropTypes.string,
};

Container.defaultProps = {
  type: 'default',
  children: null,
  className: '',
};

export default Container;

