import Icon from '../Icon/Icon';
import Link from '../Link/Link';
import styles from './Footer.scss';

export const Footer = () => (
  <footer className={styles.footer}>
    <div className={styles.textContainer}>
      <p>
        Developed by
        <Link to="https://github.com/maksymfedenko" target="_blank" className="ml-025">
          Maksym Fedenko
        </Link>
      </p>
    </div>
    <div className={styles.devLinksContainer}>
      <Link to="https://www.linkedin.com/in/maksym-fedenko/" target="_blank" className="icon-before-text">
        <Icon name="linkedIn" size="1.5rem" />
      </Link>
      <Link to="https://github.com/maksymfedenko" target="_blank">
        <Icon name="github" size="1.5rem" />
      </Link>
    </div>
  </footer>
);

export default Footer;
