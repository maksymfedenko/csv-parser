import PropTypes from 'prop-types';
import cn from 'classnames';
import styles from './Link.scss';

const Link = ({ target, to, children, className }) => (
  <a href={to} target={target} className={cn(styles.Link, className)}>
    {children}
  </a>
);

Link.propTypes = {
  target: PropTypes.string,
  to: PropTypes.string.isRequired,
  children: PropTypes.node.isRequired,
  className: PropTypes.string,
};

Link.defaultProps = {
  className: '',
  target: '_self',
};

export default Link;
