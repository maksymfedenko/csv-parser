import PropTypes from 'prop-types';
import cn from 'classnames';
import styles from './Button.scss';

const Button = ({ children, className, onClick, type, disabled }) => (
  <button
    disabled={disabled}
    className={cn(styles.Button, className)}
    onClick={onClick}
    type={type}
  >
    {children}
  </button>
);

Button.propTypes = {
  type: PropTypes.oneOf(['submit', 'reset', 'button']),
  disabled: PropTypes.bool,
  children: PropTypes.node.isRequired,
  className: PropTypes.string,
  onClick: PropTypes.func,
};

Button.defaultProps = {
  disabled: false,
  type: 'button',
  className: '',
  onClick: () => {},
};

export default Button;
