import PropTypes from 'prop-types';
import cn from 'classnames';
import styles from './ErrorsBox.scss';

const ErrorsBox = ({ className, error }) => (
  !!error && (
    <section className={cn(styles.ErrorsBox, className)}>
      <div className={styles.error}>{error}</div>
    </section>
  )
);

ErrorsBox.propTypes = {
  errors: PropTypes.string,
};

ErrorsBox.defaultProps = {
  error: '',
  className: '',
};

export default ErrorsBox;
