import PropTypes from 'prop-types';

const Form = ({ children, className, onSubmit }) => (
  <form className={className} onSubmit={onSubmit}>
    {children}
  </form>
);

Form.propTypes = {
  children: PropTypes.node.isRequired,
  className: PropTypes.string,
  onSubmit: PropTypes.func,
};

Form.defaultProps = {
  className: '',
  onSubmit: () => {},
};

export default Form;
