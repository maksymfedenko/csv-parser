/* eslint-disable react/no-array-index-key */

import PropTypes from 'prop-types';
import cn from 'classnames';
import styles from './Table.scss';

class Table extends React.PureComponent { // eslint-disable-line no-undef

  onSortChange = (index) => () => {
    const { onSort, sort: { columnIndex, order } } = this.props;

    if (index !== columnIndex) return onSort({ columnIndex: index, order: 'asc' });
    if (order === 'asc') return onSort({ columnIndex: index, order: 'desc' });
    return onSort({ columnIndex: index, order: 'asc' });
  };

  render() {
    const { className, isTableShown, columnNames, data, sort: { columnIndex, order } } = this.props;

    return (
      <div className={cn(styles.Table, className, { [styles.show]: isTableShown })}>
        <div className={styles.tableWrapper}>
          <table className={styles.table}>
            <thead>
              <tr>
                {columnNames.map((columnName, index) => (
                  <th
                    className={cn({ [`${styles.sorted} ${styles[order]}`]: index === columnIndex })}
                    scope="col"
                    key={`${columnName}_${Math.random()}`}
                    onClick={this.onSortChange(index)}
                  >
                    {columnName}
                  </th>
                ))}
              </tr>
            </thead>
            <tbody>
              {data.map((rowData) => (
                <tr key={Math.random()}>
                  {/**
                   * We iterates on columnNames instead of rowData
                   * because length of rowData can be different from columnNames
                   * So, at this way we can avoid too many, or not enough rendered td's at one row
                   * At this way we guaranteed that td's count always will be the same as
                   * columnNames count
                   */}
                  {columnNames.map((columnName, index) => (
                    <td
                      key={`${columnName}_${rowData[index]}_${Math.random()}`}
                      className={cn({ [styles.sorted]: index === columnIndex })}
                    >
                      {rowData[index]}
                    </td>
                  ))}
                </tr>
              ))}
            </tbody>
          </table>
        </div>
      </div>
    );
  }
}

Table.propTypes = {
  className: PropTypes.string,
  isTableShown: PropTypes.bool.isRequired,
  columnNames: PropTypes.arrayOf(PropTypes.string),
  data: PropTypes.arrayOf(PropTypes.arrayOf(PropTypes.string)),
  sort: PropTypes.shape({
    columnIndex: PropTypes.number,
    order: PropTypes.oneOf(['asc', 'desc']),
  }).isRequired,
  onSort: PropTypes.func.isRequired,
};

Table.defaultProps = {
  className: '',
  columnNames: [],
  data: [],
};

export default Table;
