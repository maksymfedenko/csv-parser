import PropTypes from 'prop-types';
import cn from 'classnames';
import styles from './Icon.scss';

const Icon = ({ size, name, color, className }) => (
  <i
    className={cn(styles.Icon, `icon-${name}`, className)}
    style={{ fontSize: size, height: size, width: size, color }}
  />
);

Icon.propTypes = {
  name: PropTypes.string.isRequired,
  size: PropTypes.string,
  color: PropTypes.string,
  className: PropTypes.string,
};

Icon.defaultProps = {
  color: 'white',
  size: '1rem',
  className: '',
};

export default Icon;
