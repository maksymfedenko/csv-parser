import cn from 'classnames';
import Icon from '../Icon/Icon';
import styles from './Header.scss';

export const Header = () => (
  <header className={styles.header}>
    <Icon name="csv2" className={cn('icon-before-text', styles.logo)} size="2rem" />
    <h1 className={styles.title}>CSV Parsing Service</h1>
  </header>
);

export default Header;
