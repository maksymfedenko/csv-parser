import { Header, Footer } from 'components';
import PropTypes from 'prop-types';
import styles from './DefaultLayout.scss';

export const DefaultLayout = ({ children }) => (
  <div className={styles.DefaultLayout}>
    <Header />
    <section className={styles.mainSection}>
      {children}
    </section>
    <Footer />
  </div>
);

DefaultLayout.propTypes = {
  children: PropTypes.node.isRequired,
};

export default DefaultLayout;
