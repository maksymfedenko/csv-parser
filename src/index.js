import React from 'react';
import { Provider } from 'react-redux';
import { DefaultLayout } from 'layouts';
import { CsvParser } from 'pages';
import { render } from 'react-dom';
import store from 'config/store';
import 'normalize.css';
import 'theme/global.scss';

render(
  <Provider store={store}>
    <DefaultLayout>
      <CsvParser />
    </DefaultLayout>
  </Provider>,
  document.getElementById('root'),
);
