import { CsvForm, CsvTable } from 'containers';
import { Container } from 'components';

export const CsvParser = () => (
  <Container>
    <CsvForm />
    <CsvTable />
  </Container>
);

export default CsvParser;
