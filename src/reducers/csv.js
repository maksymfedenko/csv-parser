import sortCsv from 'utils/sortCsv';

export const types = {
  CHANGE_DATA: 'csv/CHANGE_DATA',
  SUBMIT: 'csv/SUBMIT',
  CHECK_DATA: 'csv/CHECK_DATA',
  CHECK_DATA_SUCCESS: 'csv/CHECK_DATA_SUCCESS',
  CHECK_DATA_FAILED: 'csv/CHECK_DATA_FAILED',
  PARSE_DATA: 'csv/PARSE_DATA',
  CHANGE_SORT: 'csv/CHANGE_SORT',
};

const initialState = {
  isSubmitting: false,
  isSubmitted: false,
  inputtedData: '',
  parsedData: {},
  currentSort: {},
  error: '',
};

export const updateInputtedData = (data) => ({
  type: types.CHANGE_DATA,
  payload: data,
});

export const submit = () => ({
  type: types.SUBMIT,
});

export const checkData = (payload) => ({
  type: types.CHECK_DATA,
  payload,
});

export const checkDataSuccess = (payload) => ({
  type: types.CHECK_DATA_SUCCESS,
  payload,
});

export const checkDataFailed = (error) => ({
  type: types.CHECK_DATA_FAILED,
  payload: error,
});

export const parseData = (parsedData) => ({
  type: types.PARSE_DATA,
  payload: parsedData,
});

export const changeSort = (payload) => ({
  type: types.CHANGE_SORT,
  payload,
});

export default (state = initialState, { type, payload }) => {
  switch (type) {
    case types.CHANGE_DATA:
      return {
        ...state,
        inputtedData: payload,
      };

    case types.SUBMIT:
      return {
        ...state,
        isSubmitting: true,
      };

    case types.CHECK_DATA_SUCCESS:
      return {
        ...state,
        isSubmitted: true,
        isSubmitting: false,
        error: '',
      };

    case types.CHECK_DATA_FAILED:
      return {
        ...state,
        isSubmitting: false,
        error: payload,
      };

    case types.PARSE_DATA:
      return {
        ...state,
        parsedData: payload,
        currentSort: {},
      };

    case types.CHANGE_SORT:
      return {
        ...state,
        currentSort: payload,
        parsedData: {
          ...state.parsedData,
          data: sortCsv(state.parsedData.data, payload),
        },
      };

    default:
      return state;
  }
};
