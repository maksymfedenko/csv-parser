import { fork, all } from 'redux-saga/effects';
import csv from './csv';

export default function* root() {
  yield all([
    fork(csv),
  ]);
}
