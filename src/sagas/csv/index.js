import { put, takeLatest, throttle, fork, all, select } from 'redux-saga/effects';
import { types, checkData, checkDataSuccess, checkDataFailed, parseData } from 'reducers/csv';
import checkCsvForErrors from 'utils/checkCsvForErrors';
import parseCsv from 'utils/parseCsv';

function* handleDataChange(action) {
  const { isSubmitted, inputtedData } = yield select((state) => state.csv);
  const isSubmitting = action.type === types.SUBMIT;
  if (isSubmitted || isSubmitting) {
    const data = isSubmitting ? inputtedData : action.payload;
    yield put(checkData(data));
  }
}

function* handleDataCheck(action) {
  const error = checkCsvForErrors(action.payload);
  if (error) {
    yield put(checkDataFailed(error));
  } else {
    yield put(checkDataSuccess(action.payload));
  }
}

function* handleDataParse(action) {
  yield put(parseData(parseCsv(action.payload)));
}

function* dataChangeSaga() {
  yield takeLatest([types.CHANGE_DATA, types.SUBMIT], handleDataChange);
}

function* dataCheckSaga() {
  yield throttle(400, types.CHECK_DATA, handleDataCheck);
}

function* dataParseSaga() {
  yield takeLatest(types.CHECK_DATA_SUCCESS, handleDataParse);
}

function* mySaga() {
  yield all([
    fork(dataChangeSaga),
    fork(dataCheckSaga),
    fork(dataParseSaga),
  ]);
}

export default mySaga;
