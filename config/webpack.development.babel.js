/* eslint-disable import/no-extraneous-dependencies */

import webpack from 'webpack';
import path from 'path';
import merge from 'webpack-merge';
import commonConfig, { PATHS } from './webpack.common.babel';

export default merge([
  commonConfig,
  {
    devtool: 'source-map',
    module: {
      rules: [
        {
          test: /\.css$/,
          use: [
            'style-loader',
            {
              loader: 'css-loader',
              options: {
                modules: true,
                sourceMap: true,
                localIdentName: '[local]__[hash:base64:5]',
              },
            },
          ],
        }, {
          test: /\.scss$/,
          exclude: PATHS.theme,
          use: [
            'style-loader',
            {
              loader: 'css-loader',
              options: {
                modules: true,
                sourceMap: true,
                localIdentName: '[local]__[hash:base64:5]',
              },
            },
            'sass-loader',
          ],
        }, {
          test: /\.scss$/,
          include: PATHS.theme,
          use: [
            'style-loader',
            {
              loader: 'css-loader',
              options: {
                sourceMap: true,
              },
            },
            'sass-loader',
          ],
        },
      ],
    },
    plugins: [
      new webpack.DefinePlugin({
        'process.env': {
          NODE_ENV: "\"development\"", // eslint-disable-line quotes
        },
      }),
    ],
    devServer: {
      contentBase: path.join(__dirname, '../public'),
      stats: 'errors-only',
      port: 4000,
    },
  },
]);
