module.exports = {
  extends: 'airbnb',
  parser: 'babel-eslint',
  env: {
    browser: true,
    commonjs: true,
    es6: true,
    jest: true,
    node: true,
  },
  settings: {
    'import/resolver': {
      node: {
        paths: ['src'],
      },
    },
  },
  rules: {
    'arrow-parens': ['error', 'always'],
    'function-paren-newline': 'off',
    'jsx-a11y/anchor-is-valid': 'off',
    'jsx-a11y/label-has-for': 'off',
    'jsx-a11y/no-autofocus': 'off',
    'import/no-named-as-default': 'off',
    'no-shadow': 'off',
    'no-use-before-define': 'off',
    'no-restricted-globals': 'off',
    'object-curly-newline': 'off',
    'padded-blocks': 'off',
    'prefer-template': 'off',
    'react/jsx-filename-extension': 'off',
    'react/react-in-jsx-scope': 'off',
  }
};
